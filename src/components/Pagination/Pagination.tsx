import React from 'react'
import { Pagination } from 'antd'

function PaginationComp({
  currentPage,
  totalPages,
  onPageChange,
  itemsPerPage,
}) {
  const handlePageChange = (page) => {
    onPageChange(page)
  }

  return (
    <Pagination
      current={currentPage}
      total={totalPages * itemsPerPage}
      pageSize={itemsPerPage}
      onShowSizeChange={handlePageChange}
      onChange={handlePageChange}
      showLessItems // Display only 5 pages at a time
      style={{ marginTop: '20px', textAlign: 'center' }} // Add custom styling
    />
  )
}

export default PaginationComp
